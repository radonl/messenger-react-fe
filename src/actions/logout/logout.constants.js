export const [
  LOGOUT,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED
] = [
  'LOGOUT'
]