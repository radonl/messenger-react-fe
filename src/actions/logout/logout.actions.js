import simpleAction from "../../helpers/simpleAction";
import {LOGOUT} from "./logout.constants";

export const [
  logout
] = [
  simpleAction(LOGOUT)
]