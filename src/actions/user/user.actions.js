import simpleAction from "../../helpers/simpleAction";
import {AVATAR_CHANGED, PERSONAL_DATA_FORM_SUBMIT, USERNAME_CHANGED} from "./user.constants";

export const [
  personalDataFormSubmit,
  avatarChanged,
  usernameChanged
] = [
  simpleAction(PERSONAL_DATA_FORM_SUBMIT),
  simpleAction(AVATAR_CHANGED),
  simpleAction(USERNAME_CHANGED)
];