import simpleAction from "../../helpers/simpleAction";
import {CLEAR_SEARCH, SEARCH} from "./search.constants";

export const [
  search,
  clearSearch
] = [
  simpleAction(SEARCH),
  simpleAction(CLEAR_SEARCH)
]