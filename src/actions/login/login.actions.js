import simpleAction from "../../helpers/simpleAction";

import {AUTHORIZATION, CODE_CHANGED, PHONE_CHANGED, SEND_CODE} from './login.constants';

export const [
    authorization,
    codeChanged,
    phoneChanged,
    sendCode
] = [
    simpleAction(AUTHORIZATION),
    simpleAction(CODE_CHANGED),
    simpleAction(PHONE_CHANGED),
    simpleAction(SEND_CODE)
];