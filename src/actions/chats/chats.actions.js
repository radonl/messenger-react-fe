import simpleAction from "../../helpers/simpleAction";
import {
  ADD_USER_TO_GROUP, CLEAR_GROUP_CHAT,
  CREATE_CHAT, CREATE_GROUP, GROUP_REMOVE,
  LOAD_CHATS,
  PREPARE_TO_CREATE_CHAT, REMOVE_USER_FROM_GROUP,
  SELECT_CHAT,
  SEND_MESSAGE, UPDATE_GROUP_AVATAR, UPDATE_GROUP_NAME,
  WRITING_MESSAGE
} from "./chats.constants";

export const [
  loadChats,
  selectChat,
  sendMessage,
  writingMessage,
  createChat,
  prepareToCreateChat,
  createGroup,
  removeUserFromGroup,
  addUserToGroup,
  clearGroupChat,
  removeGroup,
  updateGroupName,
  updateGroupAvatar
] = [
  simpleAction(LOAD_CHATS),
  simpleAction(SELECT_CHAT),
  simpleAction(SEND_MESSAGE),
  simpleAction(WRITING_MESSAGE),
  simpleAction(CREATE_CHAT),
  simpleAction(PREPARE_TO_CREATE_CHAT),
  simpleAction(CREATE_GROUP),
  simpleAction(REMOVE_USER_FROM_GROUP),
  simpleAction(ADD_USER_TO_GROUP),
  simpleAction(CLEAR_GROUP_CHAT),
  simpleAction(GROUP_REMOVE),
  simpleAction(UPDATE_GROUP_NAME),
  simpleAction(UPDATE_GROUP_AVATAR)
];