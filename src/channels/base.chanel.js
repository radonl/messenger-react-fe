import io from "socket.io-client";

export const socket = io.connect(process.env.REACT_APP_API_URL);

class BaseChanel {
  constructor() {
    socket.emit('Authorization', JSON.parse(localStorage.getItem('session')));
  }
  subscribe(cb, event) {
    socket.on(event, cb);
  }
  unsubscribe(cb, event) {
    socket.removeListener(event, cb);
  }
}

export default new BaseChanel();