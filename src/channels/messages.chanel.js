import { eventChannel } from 'redux-saga';
import {INCOMING_MESSAGE} from "../actions/chats/chats.constants";
import baseChanel from "./base.chanel";

export default function createMessagesChanel() {
  const subscribe = emitter => {
    const cb = data => emitter(data);
    baseChanel.subscribe(cb, INCOMING_MESSAGE);
    return () => baseChanel.unsubscribe(cb, INCOMING_MESSAGE);
  };
  return eventChannel(subscribe);
}