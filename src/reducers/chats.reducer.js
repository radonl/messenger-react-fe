import {
  CHAT_CREATED,
  CHATS_LOADED,
  CLEAR_USER_WRITES_MESSAGE, GROUP_AVATAR_UPDATED,
  GROUP_CREATED,
  GROUP_NAME_UPDATED,
  GROUP_REMOVED,
  INCOMING_MESSAGE,
  INIT_MESSAGE_SENT,
  PREPARE_TO_CREATE_CHAT,
  SELECT_CHAT,
  USER_ADDED_TO_GROUP,
  USER_REMOVED_FROM_GROUP,
  USER_WRITES_MESSAGE
} from "../actions/chats/chats.constants";
import moment from "moment";
import {store} from "../store";

export const chatsReducer = (state, action, rootState) => {
  switch (action.type) {
    case CHATS_LOADED:
      return {
        ...state,
        chats: action.payload.data.map(chat => {
          if(chat.isPrivate) {
            const chatMember = chat.members.find(member => member.id !== rootState.user.id);
            chat.name = chatMember.displayedName || chatMember.username || chatMember.phone;
            chat.avatarPath = chatMember.avatarPath;
            chat.partnerId = chatMember.id;
          } else {
            chat.adminAccess = rootState.user.id === chat.ownerId;
          }
          chat.lastMessage = (chat.messages && chat.messages.length === 1) ? chat.messages[0].message : '';
          chat.lastMessageTime = (chat.messages && chat.messages.length === 1) ? moment(chat.messages[0].updatedAt).format('LT') : '';
          return chat;
        })
      };
    case INIT_MESSAGE_SENT:
    case SELECT_CHAT:
      state.chats = state.chats.map(chat => {
        chat.selected = chat.id === action.payload.id;
        return chat;
      });
      state.selectedChat = state.chats.find(chat => action.payload.id === chat.id);
      return {
        ...state
      };
    case USER_WRITES_MESSAGE:
      if (action.payload.chatId === state.selectedChat.id && action.payload.userId !== rootState.user.id) {
        clearTimeout(state.pendingToClearWritingMessage);
        state.pendingToClearWritingMessage = setTimeout(() => {
          store.dispatch({
            type: CLEAR_USER_WRITES_MESSAGE
          })
        }, 1500);
        const user = rootState.users.find(user => user.id === action.payload.userId);
        if (user) {
          state.userWritingMessage = `${user.name} is writing message...`
        }
      }
      return {
        ...state,
      };
    case CLEAR_USER_WRITES_MESSAGE:
      return {
        ...state,
        userWritingMessage: ''
      };
    case INCOMING_MESSAGE:
      return {
        ...state,
        chats: state.chats.map(chat => {
          if (chat.id === action.payload.chatId) {
            chat.lastMessage = action.payload.message;
          }
          return chat;
        })
      };
    case PREPARE_TO_CREATE_CHAT:
      return {
        ...state,
        isPrepareToCreateChat: true,
        newChatPartner: action.payload,
        selectedChat: {}
      }
    case CHAT_CREATED:
      return {
        ...state,
        chats: [...state.chats, action.payload]
      }
    case GROUP_CREATED:
      return {
        ...state,
        chats: [...state.chats, {
          ...action.payload.data,
          adminAccess: true
        }]
      }
    case USER_REMOVED_FROM_GROUP:
      return {
        ...state,
        chats: state.chats.map(chat => {
          if(chat.id === action.payload.chatId) {
            chat.members = chat.members.filter(member => member.id !== action.payload.userId);
          }
          return chat;
        })
      }
    case USER_ADDED_TO_GROUP:
      return {
        ...state,
        chats: state.chats.map(chat => {
          if(chat.id === action.payload.chatId) {
            chat.members.push(rootState.users.find(user => user.id === action.payload.userId))
          }
          return chat;
        })
      }
    case GROUP_REMOVED:
      state.chats = state.chats.filter(chat => chat.id !== action.payload);
      if(state.selectedChat.id === action.payload) {
        state.selectedChat = {}
      }
      return {
        ...state
      }
    case GROUP_NAME_UPDATED:
    case GROUP_AVATAR_UPDATED:
      state.chats = state.chats.map(chat => {
        if(chat.id === action.payload.data.id) {
          chat.name = action.payload.data.name;
          chat.avatarPath = action.payload.data.avatarPath;
        }
        return chat;
      });
      return {
        ...state
      }
    default: {
      return {...state};
    }
  }
}

export const chatsDefaultState = {
  chats: [],
  selectedChat: {},
  isPrepareToCreateChat: false,
  newChatPartner: {},
  userWritingMessage: '',
  clearWritingMessageTimeout: null,
};