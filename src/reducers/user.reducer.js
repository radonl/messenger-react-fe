import {
  AVATAR_CHANGED_SUCCESS,
  PERSONAL_DATA_SAVING_SUCCESS,
  USERNAME_CHANGED_SUCCESS
} from "../actions/user/user.constants";

export const userReducer = (state, action) => {
  switch (action.type) {
    case AVATAR_CHANGED_SUCCESS:
    case PERSONAL_DATA_SAVING_SUCCESS:
    case USERNAME_CHANGED_SUCCESS:
      localStorage.setItem('user', JSON.stringify(action.payload.data));
      return {
        ...action.payload.data
      }
    default: {
      return state;
    }
  }
};

export const userDefaultState = {
  ...JSON.parse(localStorage.getItem('user'))
};