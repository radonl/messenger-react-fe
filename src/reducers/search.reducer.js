import {CLEAR_SEARCH, SEARCH, SEARCH_RESULTS_LOADED} from "../actions/search/search.constants";
import {CHAT_CREATED} from "../actions/chats/chats.constants";

export const searchReducer = (state, action, rootState) => {
  switch (action.type) {
    case SEARCH:
      state.requestInProgress = true;
      state.displayResults = true;
      break;
    case SEARCH_RESULTS_LOADED:
      state.requestInProgress = false;
      state.displayResults = true;
      state.results = action.payload.data.map(user => {
        const fetchedPrivateChat = rootState.chats.chats.find(chat => chat.partnerId === user.id);
        if(fetchedPrivateChat) {
          return fetchedPrivateChat;
        }
        user.name = user.displayedName || user.username || user.phone;
        user.lastMessage = 'Start chatting';
        user.isSearchResult = true;
        return user;
      });
      break;
    case CLEAR_SEARCH:
    case CHAT_CREATED:
      state.displayResults = false;
      state.requestInProgress = false;
      break;
    default: {
      break;
    }
  }
  return {...state};
}

export const searchDefaultState = {
  results: [],
  requestInProgress: false,
  displayResults: false
};