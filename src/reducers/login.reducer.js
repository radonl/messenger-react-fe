import {
  AUTHORIZATION_FAILED,
  CODE_CHANGED,
  CODE_SENDING_FAILED,
  CODE_SENT,
  PHONE_CHANGED
} from "../actions/login/login.constants";

export const loginReducer = (state, action) => {
  switch (action.type) {
    case PHONE_CHANGED:
      state.phone = action.payload;
      return {...state};
    case CODE_CHANGED:
      state.code = action.payload;
      return {...state};
    case CODE_SENT:
      state.codeSent = true;
      return {...state};
    case CODE_SENDING_FAILED:
      state.sendingCodeFailed = true;
      return {...state};
    case AUTHORIZATION_FAILED:
      state.authorizationFailed = true;
      return {...state};
    default: {
      return {...state};
    }
  }
}

export const loginDefaultState = {
  phone: '',
  code: '',
  codeSent: false,
  sendingCodeFailed: false,
  authorizationFailed: false
}