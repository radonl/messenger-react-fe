import {takeEvery} from "@redux-saga/core/effects";
import simpleSaga from "../helpers/simpleSaga";
import AuthorizationService from "../services/authorization.service";
import {LOGOUT, LOGOUT_FAILED, LOGOUT_SUCCESS} from "../actions/logout/logout.constants";

export const logoutSaga = function* () {
  yield takeEvery(LOGOUT, simpleSaga(AuthorizationService.logout, LOGOUT_SUCCESS, LOGOUT_FAILED));
};