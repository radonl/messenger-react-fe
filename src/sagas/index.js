import {all, fork} from "@redux-saga/core/effects";
import {loginSaga} from "./login.saga";
import {chatsSaga} from "./chats.saga";
import chatsSocketsSaga from "./chats-sockets.saga";
import {userSaga} from './user.saga';
import {messagesSaga} from "./messages.saga";
import {searchSaga} from "./search.saga";
import {logoutSaga} from "./logout.saga";

function* rootSaga() {
  yield all([
    fork(loginSaga),
    fork(chatsSaga),
    fork(messagesSaga),
    fork(userSaga),
    fork(searchSaga),
    fork(logoutSaga),
    ...chatsSocketsSaga
  ]);
}

export default rootSaga;
