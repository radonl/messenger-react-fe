import {put, take, call, fork} from "@redux-saga/core/effects";
import createMessagesChanel from "../channels/messages.chanel";
import createWritesMessageChanel from "../channels/user-writes-message.chanel";
import {INCOMING_MESSAGE, USER_WRITES_MESSAGE} from "../actions/chats/chats.constants";

const incomingMessage = function* () {
  const channel = yield call(createMessagesChanel);
  while (true) {
    const result = yield take(channel);
    yield put({
      type: INCOMING_MESSAGE,
      payload: result
    })
  }
};

const userWritesMessage = function* () {
  const channel = yield call(createWritesMessageChanel);
  while (true) {
    const result = yield take(channel);
    yield put({
      type: USER_WRITES_MESSAGE,
      payload: result
    })
  }
};

export default [
  fork(incomingMessage),
  fork(userWritesMessage)
];