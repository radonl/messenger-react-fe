import {takeEvery} from "@redux-saga/core/effects";
import {
    AVATAR_CHANGED, AVATAR_CHANGED_SUCCESS, AVATAR_CHANGING_FAILED,
    PERSONAL_DATA_FORM_SUBMIT,
    PERSONAL_DATA_SAVING_FAILED,
    PERSONAL_DATA_SAVING_SUCCESS,
    USERNAME_CHANGED,
    USERNAME_CHANGED_SUCCESS,
    USERNAME_CHANGING_FAILED
} from "../actions/user/user.constants";
import simpleSaga from "../helpers/simpleSaga";
import SettingsService from "../services/settings.service";

export const userSaga = function* () {
    yield takeEvery(PERSONAL_DATA_FORM_SUBMIT, simpleSaga(
        SettingsService.updateUser,
        PERSONAL_DATA_SAVING_SUCCESS,
        PERSONAL_DATA_SAVING_FAILED
    ));
    yield takeEvery(AVATAR_CHANGED, simpleSaga(
        SettingsService.updateUser,
        AVATAR_CHANGED_SUCCESS,
        AVATAR_CHANGING_FAILED
    ));
    yield takeEvery(USERNAME_CHANGED, simpleSaga(
        SettingsService.updateUser,
        USERNAME_CHANGED_SUCCESS,
        USERNAME_CHANGING_FAILED
    ));
}