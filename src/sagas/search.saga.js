import {takeEvery} from "@redux-saga/core/effects";
import simpleSaga from "../helpers/simpleSaga";
import chatsService from "../services/chats.service";
import {SEARCH, SEARCH_FAILED, SEARCH_RESULTS_LOADED} from "../actions/search/search.constants";

export const searchSaga = function* () {
  yield takeEvery(SEARCH, simpleSaga(chatsService.search, SEARCH_RESULTS_LOADED, SEARCH_FAILED))
}