const apiUrl = process.env.REACT_APP_API_URL;

class Api {
  async _call({ url, method = "GET", body = null }) {
    const session = JSON.parse(localStorage.getItem("session"));
    const options = {
      method,
      headers: {
        "Content-Type": "application/json",
        ...session ? { Authorization: session.token } : {}
      }
    };

    if (body) {
      options.body = JSON.stringify(body);
    }

    try {
      const response = await fetch(`${apiUrl}/${url}`, options);
      const responseBody = await response.json();

      if (!response.ok) {
        if(response.status === 401) {
          localStorage.removeItem('session');
          localStorage.removeItem('user');
          window.location.reload();
        }
        return Promise.reject({
          body: responseBody,
          message: responseBody.message,
          status: response.status
        });
      }

      return responseBody;
    } catch (err) {
      throw new err;
    }
  }

  get(url: string): Promise<any> {
    return this._call({ url });
  }

  post(url: string, body: any): Promise<any> {
    return this._call({ url, method: "POST", body });
  }

  put(url: string, body: any): Promise<any> {
    return this._call({ url, method: "PUT", body });
  }

  delete(url: string, body?: any): Promise<any> {
    return this._call({ url, method: "DELETE", body });
  }
}

export default new Api();
