import Api from './api.service';

class SettingsService {
  async updateUser({displayedName, avatar, username}) {
    return Api.put('users', {
      displayedName,
      avatar,
      username
    });
  }
}

export default new SettingsService();