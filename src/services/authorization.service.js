import Api from './api.service';
import jwt_decode from 'jwt-decode';

class AuthorizationService {
  async sendCode({phone}) {
    try {
      return await Api.post('sendVerificationCode', {
        phone: `380${phone}`
      })
    } catch (e) {
      throw e;
    }
  }
  async login({phone, code}) {
    try {
      const result = await Api.post('login', {
        phone: `380${phone}`,
        code
      });
      localStorage.setItem('session', JSON.stringify(result.data.session));
      localStorage.setItem('user', JSON.stringify(result.data.user));
      window.location.reload();
    } catch (e) {
      throw e;
    }
  }
  async logout() {
    try {
      await Api.post('logout');
      localStorage.removeItem('session');
      localStorage.removeItem('user');
      window.location.reload();
    } catch (e) {
      throw e;
    }
  }

  isAuthorized() {
    const session = JSON.parse(localStorage.getItem("session"));
    if(session) {
      const token = session.token;
      if(token) {
        const decoded = jwt_decode(token);
        if (decoded.exp !== undefined) {
          const date = new Date(0);
          date.setUTCSeconds(decoded.exp);
          return (date.valueOf() > new Date().valueOf())
        }
      }
    }
    return false;
  }
}

export default new AuthorizationService();