import React from 'react';
import './assets/main.css'
import 'emoji-mart/css/emoji-mart.css';
import Routing from "./components/Routing/Routing";

function App() {
  return (
    <Routing/>
  );
}

export default App;
