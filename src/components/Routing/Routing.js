import React from "react";
import {Redirect, Route, Router, Switch} from "react-router-dom";

import {history} from "../../helpers/history";
import LoginPage from "../../pages/Login/LoginPage";
import AuthorizationService from '../../services/authorization.service';
import MainPage from "../../pages/Main/MainPage";

const Routing = () => {
  const isAuthorized = AuthorizationService.isAuthorized();
  return <Router history={history}>
    <Route render={({location}) => (
      <div className="App">
        <Switch location={location}>
          {isAuthorized && <Route path={'/messages'} component={MainPage}/>}
          {!isAuthorized && <Route path={'/login'} component={LoginPage}/>}
          <Redirect from="*" to={isAuthorized ? '/messages' : '/login'} />
        </Switch>
      </div>
    )} />
  </Router>
};

export default Routing;