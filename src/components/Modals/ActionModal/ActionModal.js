import Dialog from "@material-ui/core/Dialog";
import React from "react";
import {DialogActions} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";

const ActionModal = ({open, handleCancel, handleOk, title}) => {
  return <Dialog open={open} onClose={handleCancel}>
    <DialogTitle>{title}</DialogTitle>
    <DialogActions>
      <Button onClick={() => handleOk()}>OK</Button>
      <Button onClick={() => handleCancel()}>CANCEL</Button>
    </DialogActions>
  </Dialog>
};

export default ActionModal;