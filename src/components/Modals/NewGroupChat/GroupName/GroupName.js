import React, {useState} from "react";
import {Dialog, DialogActions, DialogTitle} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

const GroupName = ({open, handleClose, handleCreate}) => {
  const [name, setName] = useState('');
  return <Dialog open={open} onClose={handleClose}>
    <DialogTitle>Create Group</DialogTitle>
    <DialogContent>
      <TextField value={name} onChange={e => setName(e.target.value)}/>
    </DialogContent>
    <DialogActions>
      <Button onClick={() => handleClose()}>Cancel</Button>
      <Button onClick={() => {
        handleCreate(name);
        setName('');
      }}>Create</Button>
    </DialogActions>
  </Dialog>
}
export default GroupName;