import React, {useEffect, useState} from "react";
import {Dialog} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import {useDispatch, useSelector} from "react-redux";
import Chat from "../../../pages/Main/Chats/Chat/Chat";
import {MD5} from "object-hash";
import Button from "@material-ui/core/Button";
import GroupName from "./GroupName/GroupName";
import {createGroup} from "../../../actions/chats/chats.actions";

const NewGroupChat = ({open, handleClose}) => {
  const users = useSelector(state => state.users);
  const me = useSelector(state => state.user);
  const [innerUsers, setInnerUsers] = useState([]);
  const [ableToNext, setAbleToNext] = useState(false);
  const [groupNameIsOpen, setGroupNameIsOpen] = useState(false);
  const [search, setSearch] = useState('');
  const dispatch = useDispatch();
  useEffect(() => {
    const searchLowercase = search.toLocaleLowerCase();
    setInnerUsers(users.filter(user => {
      return user.id !== me.id && (me) && (
        user.displayedName.toLocaleLowerCase().includes(searchLowercase) ||
        user.phone.toLocaleLowerCase().includes(searchLowercase) ||
        user.username.toLocaleLowerCase().includes(searchLowercase)
      )
    }).map(user => {
      const innerUser = innerUsers.find(iUser => iUser.id === user.id);
      user.selected = innerUser && innerUser.selected;
      return user;
    }))
  }, [users, search]);
  const clickOnUser = id => {
    setInnerUsers(innerUsers.map(user => {
      if (user.id === id) {
        user.selected = !user.selected;
      }
      return user;
    }))
    setAbleToNext(innerUsers.filter(iUser => iUser.selected).length > 0);
  };
  const closeDialog = () => {
    setInnerUsers(innerUsers.map(iUser => {
      iUser.selected = false;
      return iUser;
    }));
    setGroupNameIsOpen(false);
    handleClose();
  };
  const handleCreateDialog = name => {
    dispatch(createGroup({name, users: innerUsers.filter(u => u.selected).map(u => u.id)}));
    closeDialog();
  };
  return <Dialog open={open} onClose={() => closeDialog()}>
    <div className='w-530'>
      <div className='flex flex-col bg-blue-600 text-gray-100 p-3 justify-between'>
        <div className='flex'>
          <span>New group</span>
          <div className='flex flex-grow justify-end'>
            <span className='ml-2 cursor-pointer' onClick={() => closeDialog()}>Close</span>
          </div>
        </div>
      </div>
      <div className="relative">
        <div className="absolute ml-5">
          <SearchIcon className='text-gray-400 mt-2'/>
        </div>
        <input type="text"
               className="bg-gray-200 p-1 pl-12 pt-2 pb-2 w-full rounded focus:bg-white focus"
               placeholder="Search"
               value={search}
               onChange={e => setSearch(e.target.value)}
        />
      </div>
      <div>
        {innerUsers.map(user => {
          return <Chat chat={user}
                       key={MD5(user)}
                       isNewGroup={true}
                       onClick={id => clickOnUser(id)}/>
        })}
      </div>
      <div className='p-2 flex justify-end'>
        <Button color="primary" disabled={!ableToNext} onClick={() => setGroupNameIsOpen(true)}>Next</Button>
      </div>
      <GroupName open={groupNameIsOpen} handleCreate={name => handleCreateDialog(name)}
                 handleClose={() => setGroupNameIsOpen(false)}/>
    </div>
  </Dialog>
};

export default NewGroupChat;