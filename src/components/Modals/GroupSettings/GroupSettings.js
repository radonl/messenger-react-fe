import Dialog from "@material-ui/core/Dialog";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import User from "./User/User";
import {MD5} from "object-hash";
import Button from "@material-ui/core/Button";
import AddUser from "./AddUser/AddUser";
import ActionModal from "../ActionModal/ActionModal";
import {
  clearGroupChat,
  removeGroup,
  removeUserFromGroup,
  updateGroupAvatar,
  updateGroupName
} from "../../../actions/chats/chats.actions";
import UpdateGroupName from "./UpdateGroutName/UpdateGroupName";
import EditAvatar from "../Settings/EditAvatar/EditAvatar";

const GroupSettings = ({open, handleClose}) => {
  const {selectedChat} = useSelector(state => state.chats);
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();
  const [addUserIsOpen, setAddUserIsOpen] = useState(false);
  const [clearChatIsOpen, setClearChatIsOpen] = useState(false);
  const [removeChatIsOpen, setRemoveChatIsOpen] = useState(false);
  const [updateGroupNameIsOpen, setUpdateGroupNameIsOpen] = useState(false);
  useEffect(() => {
    setAddUserIsOpen(false);
    setClearChatIsOpen(false);
    setRemoveChatIsOpen(false);
    setUpdateGroupNameIsOpen(false);
  }, [open])
  return <Dialog open={open} onClose={handleClose}>
    <div className='w-530 relative'>
      <div className='flex flex-col bg-blue-600 text-gray-100 p-5 h-32 justify-between'>
        <div className='flex'>
          <span>Settings</span>
          <div className='flex flex-grow justify-end'>
            <span className='cursor-pointer' onClick={() => setUpdateGroupNameIsOpen(true)}>Edit</span>
            <span className='ml-2 cursor-pointer' onClick={handleClose}>Close</span>
          </div>
        </div>
        <div className='flex w-48 justify-evenly'>
          <div className="rounded-full bg-gray-400 w-12 h-12 bg-cover"
               style={{backgroundImage: `url(${selectedChat.avatarPath})`}}/>
          <div>
            <div className="text-white">{selectedChat.name}</div>
            <div>{selectedChat.members && <span className='text-gray-500'>
              {selectedChat.members.length} members
            </span>}</div>
          </div>
        </div>
      </div>
      <EditAvatar onChangeAvatar={avatar => dispatch(updateGroupAvatar({
        id: selectedChat.id,
        avatar,
        name: selectedChat.name
      }))}/>
      <div className='flex justify-center w-100'>
        <div className='flex flex-col w-3/4'>
          {selectedChat.members && selectedChat.members.map(user => {
            return <User user={user}
                         adminAccess={selectedChat.adminAccess}
                         key={MD5(user)}
                         ownerId={selectedChat.ownerId}
                         handleRemove={id => dispatch(removeUserFromGroup({
                           userId: id,
                           chatId: selectedChat.id
                         }))}
            />
          })}
        </div>
      </div>
      {selectedChat.ownerId === user.id && <div className='flex justify-evenly p-2'>
        <Button onClick={() => setAddUserIsOpen(true)}>Add user</Button>
        <Button onClick={() => setClearChatIsOpen(true)}>Clear chat</Button>
        <Button onClick={() => setRemoveChatIsOpen(true)}>Remove chat</Button>
      </div>}
      <AddUser open={addUserIsOpen} handleClose={() => setAddUserIsOpen(false)} members={selectedChat.members}/>
      <ActionModal
        open={removeChatIsOpen}
        handleCancel={() => setRemoveChatIsOpen(false)}
        handleOk={() => {
          dispatch(removeGroup(selectedChat.id));
          handleClose();
        }}
        title={`Are you sure, that you want remove "${selectedChat.name}" group?`}
      />
      <ActionModal
        open={clearChatIsOpen}
        handleCancel={() => setClearChatIsOpen(false)}
        handleOk={() => {
          dispatch(clearGroupChat(selectedChat.id));
          handleClose();
        }}
        title={`Are you sure, that you want clear messages in "${selectedChat.name}" group?`}
      />
      <UpdateGroupName
        open={updateGroupNameIsOpen}
        handleClose={() => setUpdateGroupNameIsOpen(false)}
        handleSave={name => {
          dispatch(updateGroupName({
            id: selectedChat.id,
            name
          }));
          setUpdateGroupNameIsOpen(false);
        }}
      />
    </div>
  </Dialog>
};

export default GroupSettings;
