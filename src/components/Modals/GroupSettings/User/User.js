import React, {useState} from "react";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import ActionModal from "../../ActionModal/ActionModal";

const User = ({user, handleRemove, adminAccess, ownerId, handleClick}) => {
  const [removeUserModalIsOpen, setRemoveUserModalIsOpen] = useState(false);
  return <div
    className='hover:bg-indigo-200 flex p-3'
    onClick={() => handleClick && handleClick()}
  >
    <div className="rounded-full bg-gray-400 w-12 h-12 bg-cover" style={{backgroundImage: `url(${user.avatarPath})`}}/>
    <div className="flex justify-between flex-grow ml-5">
      <div className="flex items-center">
        <span>{user.name} {user.id === ownerId && '(Administrator)'}</span>
      </div>
      {adminAccess && user.id !== ownerId  && <div className='flex items-center text-red-800'>
        <DeleteOutlineIcon className='cursor-pointer' onClick={() => setRemoveUserModalIsOpen(true)}/>
        <ActionModal
          title={`Are you sure, that you want remove ${user.name}`}
          open={removeUserModalIsOpen}
          handleCancel={() => setRemoveUserModalIsOpen(false)}
          handleOk={() => handleRemove(user.id)}
        />
      </div>}
    </div>
  </div>
};

export default User;