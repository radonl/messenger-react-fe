import Dialog from "@material-ui/core/Dialog";
import {DialogContent, DialogTitle} from "@material-ui/core";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import User from "../User/User";
import {MD5} from "object-hash";
import {addUserToGroup} from "../../../../actions/chats/chats.actions";

const AddUser = ({open, handleClose, members}) => {
  const users = useSelector(state => state.users);
  const {selectedChat} = useSelector(state => state.chats);
  const dispatch = useDispatch();
  const [availableUsers, setAvailableUsers] = useState([]);
  useEffect(() => {
    if(members) {
      setAvailableUsers(users.filter(user => !members.find(member => member.id === user.id)))
    }
  }, [members])
  return <Dialog open={open} onClose={handleClose}>
    <DialogTitle>Add user</DialogTitle>
    <DialogContent>
      <div className='flex justify-center w-310'>
        <div className='flex flex-col w-3/4'>
          {availableUsers && availableUsers.map(user => {
            return <User user={user}
                         key={MD5(user)}
                         handleClick={() => {
                           dispatch(addUserToGroup({userId: user.id, chatId: selectedChat.id}));
                           handleClose();
                         }}
            />
          })}
        </div>
      </div>
    </DialogContent>
  </Dialog>
}

export default AddUser;