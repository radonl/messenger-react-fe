import {Dialog, DialogTitle, TextField} from "@material-ui/core";
import React, {useState} from "react";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";

const UpdateGroupName = ({open, handleClose, handleSave}) => {
  const [name, setName] = useState('');
  return <Dialog open={open} onClose={handleClose}>
    <DialogTitle>Set new group name</DialogTitle>
    <DialogContent>
      <TextField label="Name" value={name} onChange={e => setName(e.target.value)}/>
    </DialogContent>
    <DialogActions>
      <Button onClick={handleClose}>Cancel</Button>
      <Button onClick={() => handleSave(name)} disabled={name.length === 0}>Save</Button>
    </DialogActions>
  </Dialog>
};

export default UpdateGroupName;
