import React, {useState} from "react";
import PhotoCameraIcon from "@material-ui/icons/PhotoCamera";
import {useDispatch} from "react-redux";
import {avatarChanged} from "../../../../actions/user/user.actions";

const EditAvatar = ({onChangeAvatar}) => {
  const [avatar, setAvatar] = useState( {
    fileContent: '',
    fileExtension: 'png'
  });
  const changeAvatar = (e) => {
    let file = e.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const avatar = {
        fileContent: reader.result,
        fileExtension: file.name.split('.').pop()
      };
      onChangeAvatar({
        ...avatar,
        fileContent: avatar.fileContent.split(',')[1],
      });
      setAvatar(avatar);
    };
    reader.onerror = () => {
      console.error(reader.error);
    };
  };
  return <div className="flex items-center">
    <label htmlFor="avatar" className={`flex justify-center items-center bg-cover rounded-full bg-white shadow-2xl border-2 border-solid w-12 h-12 absolute select-avatar`}>
      <PhotoCameraIcon className='text-gray-500 absolute'/>
    </label>
    <input type="file" id='avatar' onChange={e => changeAvatar(e)} className='hidden'/>
  </div>

};

export default EditAvatar;