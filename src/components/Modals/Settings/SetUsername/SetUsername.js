import {Dialog, DialogActions} from "@material-ui/core";
import React, {useState} from "react";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import {useDispatch} from "react-redux";
import {personalDataFormSubmit, usernameChanged} from "../../../../actions/user/user.actions";

const SetUsername = ({open, handleClose}) => {
  const dispatch = useDispatch();
  const [username, setUsername] = useState('');
  return <Dialog open={open} onClose={handleClose}>
    <DialogContent>
      <strong>Change username</strong>
      <div className='flex flex-col'>
        <TextField label='Username' value={username} onChange={e => setUsername(e.target.value)}/>
      </div>
      <div className="w-48 mt-6">
          You can choose a username on Messenger.
          If you do, other people will be able to find you by
          this username and contact you without knowing your phone number.
      </div>
      <div className='w-48 mt-2'>
        You can use a-z, 0-9 and underscores. Minimum length is 5 characters.
      </div>
    </DialogContent>
    <DialogActions>
      <Button color="primary" onClick={handleClose}>Cancel</Button>
      <Button color="primary" onClick={() => {
        dispatch(usernameChanged({
          username
        }));
        handleClose();
      }}>Save</Button>
    </DialogActions>
  </Dialog>
};

export default SetUsername;