import {Dialog} from "@material-ui/core";
import React, {useState} from "react";
import PhoneIcon from '@material-ui/icons/Phone';
import EditPersonalData from "./EditPersonalData/EditPersonalData";
import {useDispatch, useSelector} from "react-redux";
import EditAvatar from "./EditAvatar/EditAvatar";
import SetUsername from "./SetUsername/SetUsername";
import {avatarChanged} from "../../../actions/user/user.actions";

const Settings = ({open, handleClose}) => {
  const [editPersonalDataIsOpen, setEditPersonalDataIsOpen] = useState(false);
  const [editUsernameIsOpen, setEditUsernameIsOpen] = useState(false);
  const user = useSelector(
      state => state.user
  );
  const dispatch = useDispatch();
  return <Dialog open={open} onClose={handleClose}>
    <div className='w-530 relative'>
      <div className='flex flex-col bg-blue-600 text-gray-100 p-5 h-32 justify-between'>
        <div className='flex'>
          <span>Settings</span>
          <div className='flex flex-grow justify-end'>
            <span className='cursor-pointer' onClick={() => setEditPersonalDataIsOpen(true)}>Edit</span>
            <span className='ml-2 cursor-pointer'>Close</span>
          </div>
        </div>
        <div className='flex w-48 justify-evenly'>
          <div className="rounded-full bg-gray-400 w-12 h-12 bg-cover" style={{backgroundImage: `url(${user.avatarPath})`}} />
          <div>
            <div>{user.displayedName}</div>
            <div className='text-gray-300'>online</div>
          </div>
        </div>
      </div>
      <EditAvatar onChangeAvatar={avatar => dispatch(avatarChanged({avatar}))}/>
      <div className='p-8'>
        <div className='flex w-full'>
          <div className='flex w-16 justify-center'>
            <PhoneIcon className='text-gray-500'/>
          </div>
          <div className='flex flex-col'>
            <span>+{user.phone}</span>
            <small className='text-gray-700'>Phone</small>
          </div>
        </div>
        <div className='flex w-full mt-2'>
          <div className='w-16' />
          <div className='flex flex-col'>
            <span className="set-username" onClick={() => setEditUsernameIsOpen(true)}>
              {user.username ? user.username : 'Set username'}
            </span>
            <small className='text-gray-700'>Username</small>
          </div>
        </div>
      </div>
      <EditPersonalData open={editPersonalDataIsOpen} handleClose={() => setEditPersonalDataIsOpen(false)}/>
      <SetUsername open={editUsernameIsOpen} handleClose={() => setEditUsernameIsOpen(false)}/>
    </div>
  </Dialog>
};

export default Settings;