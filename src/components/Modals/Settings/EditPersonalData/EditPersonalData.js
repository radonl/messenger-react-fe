import {Dialog, DialogActions} from "@material-ui/core";
import React, {useState} from "react";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import Button from "@material-ui/core/Button";
import {useDispatch} from "react-redux";
import {personalDataFormSubmit} from "../../../../actions/user/user.actions";

const EditPersonalData = ({open, handleClose}) => {
  const dispatch = useDispatch();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  return <Dialog open={open} onClose={handleClose}>
    <DialogContent>
      <strong>Edit profile</strong>
      <div className='flex flex-col h-32'>
        <TextField label='First Name' value={firstName} onChange={e => setFirstName(e.target.value)}/>
        <TextField label='Last Name' value={lastName} onChange={e => setLastName(e.target.value)}/>
      </div>
    </DialogContent>
    <DialogActions>
      <Button color="primary" onClick={handleClose}>Cancel</Button>
      <Button color="primary" onClick={() => {
        dispatch(personalDataFormSubmit({
          displayedName: `${firstName} ${lastName}`
        }));
        handleClose();
      }}>Save</Button>
    </DialogActions>
  </Dialog>
};

export default EditPersonalData;