import React from "react";
import Search from "./Search/Search";
import Chat from "./Chat/Chat";
import {MD5} from "object-hash";
import {useSelector} from "react-redux";
import ClipLoader from "react-spinners/ClipLoader";

const Chats = () => {
  const {chats} = useSelector(state => state.chats);
  const {displayResults, results, requestInProgress} = useSelector(state => state.search);
  return <div className='w-310 border-r-4 chats-height'>
    <Search class="p-3"/>
    {displayResults ? <div className='flex justify-center'>
      {requestInProgress ? <ClipLoader color='#2b6cb0'/>:
        (results.length === 0 ?
          <span>No matches found!</span>: <div className='w-310'>
              {results.map(chat => <Chat chat={chat} key={MD5(chat)}/>)}
          </div>
        )}
    </div> : (chats && chats.map((chat, index) => (
      <Chat chat={chat} key={MD5(chat)}/>
    )))}
  </div>
};

export default Chats;