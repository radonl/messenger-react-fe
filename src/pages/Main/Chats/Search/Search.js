import React from "react";
import SearchIcon from '@material-ui/icons/Search';
import {useDispatch} from "react-redux";
import {clearSearch, search} from '../../../../actions/search/search.actions';

const Search = () => {
  const dispatch = useDispatch();
  return <div className="relative p-5">
    <div className="absolute ml-1">
      <SearchIcon className='text-gray-400 mt-1'/>
    </div>
    <input type="text"
           className="bg-gray-200 p-1 pl-8 w-full rounded focus:bg-white focus"
           placeholder="Search" onChange={e => dispatch(e.target.value ? search(e.target.value) : clearSearch())}
    />
  </div>
};

export default Search;