import React from "react";
import {prepareToCreateChat, selectChat} from "../../../../actions/chats/chats.actions";
import {connect, useDispatch} from "react-redux";
import {Check} from "@material-ui/icons";

const Chat = ({chat, isNewGroup, selected, onClick}) => {
  const dispatch = useDispatch();
  const handleClick = () => {
    if (isNewGroup) {
      onClick(chat.id);
    } else {
      dispatch(chat.isSearchResult ? prepareToCreateChat(chat) : selectChat({...chat}))
    }
  }
  return <div
    className={`${chat.selected ? 'bg-teal-900 text-white' : 'hover:bg-indigo-200'} flex p-3 justify-between`}
    onClick={() => handleClick()}
  >
    <div className="rounded-full bg-gray-400 w-12 h-12 bg-cover" style={{backgroundImage: `url(${chat.avatarPath})`}}/>
    <div className="flex flex-col">
      <span className='w-165 truncate'>{chat.name}</span>
      <span className="mt-1 w-165 truncate">{chat.lastMessage}</span>
    </div>
    <div>
      <span className="text-xs">{chat.lastMessageTime}</span>
    </div>
      {isNewGroup && chat.selected && <div>
          <Check />
      </div>}
  </div>
};

export default Chat;