import React, {useEffect, useState} from "react";
import MenuIcon from '@material-ui/icons/Menu';
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {useDispatch, useSelector} from "react-redux";

import Chats from "./Chats/Chats";
import Messages from "./Messages/Messages";
import Settings from "../../components/Modals/Settings/Settings";
import {loadChats} from "../../actions/chats/chats.actions";
import NewGroupChat from "../../components/Modals/NewGroupChat/NewGroupChat";
import {logout} from "../../actions/logout/logout.actions"
import Header from "./Header/Header";

const MainPage = () => {

    const dispatch = useDispatch();
    const {selectedChat, isPrepareToCreateChat} = useSelector(state => state.chats);
    useEffect(() => {
        dispatch(loadChats())
    },[]);
    return <div className="flex justify-center bg-gray-200 h-screen">
        <div className="flex flex-col w-1010 mb-5 bg-white shadow-2xl">
            <Header />
            <div className="flex">
                <Chats/>
                {(selectedChat.id || isPrepareToCreateChat) && <Messages className="w-full"/>}
            </div>
        </div>
    </div>
};

export default MainPage;