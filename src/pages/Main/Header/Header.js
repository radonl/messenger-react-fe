import React, {useState} from "react";
import MenuIcon from "@material-ui/icons/Menu";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {logout} from "../../../actions/logout/logout.actions";
import Settings from "../../../components/Modals/Settings/Settings";
import NewGroupChat from "../../../components/Modals/NewGroupChat/NewGroupChat";
import {useDispatch, useSelector} from "react-redux";
import GroupSettings from "../../../components/Modals/GroupSettings/GroupSettings";

const Header = () => {
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  const [menuIconRef, setMenuIconRef] = useState();
  const [settingsIsOpen, setSettingsIsOpen] = useState(false);
  const [newGroupIsOpen, setNewGroupIsOpen] = useState(false);
  const [groupSettingsIsOpen, setGroupSettingsIsOpen] = useState(false);
  const {selectedChat} = useSelector(state => state.chats);
  const dispatch = useDispatch();
  return <div className="bg-teal-900 h-12 w-full d-flex">
    <div className="flex items-center h-full">
      <div className="w-310">
        <MenuIcon className='ml-5 cursor-pointer text-white' ref={ref => setMenuIconRef(ref)}
                  onClick={() => setMenuIsOpen(true)}/>
        <Menu
          id="simple-menu"
          anchorEl={menuIconRef}
          keepMounted
          open={menuIsOpen}
          onClose={() => setMenuIsOpen(false)}
        >
          <MenuItem onClick={() => {
            setMenuIsOpen(false);
            setNewGroupIsOpen(true);
          }}>New group</MenuItem>
          <MenuItem onClick={() => {
            setMenuIsOpen(false);
            setSettingsIsOpen(true);
          }}>Settings</MenuItem>
          <MenuItem onClick={() => dispatch(logout())}>Logout</MenuItem>
        </Menu>
        <Settings open={settingsIsOpen} handleClose={() => setSettingsIsOpen(false)}/>
        <NewGroupChat open={newGroupIsOpen} handleClose={() => setNewGroupIsOpen(false)}/>
      </div>
      {selectedChat && selectedChat.id &&
      <div className='w-full hover:bg-teal-800 h-full flex items-center justify-center cursor-pointer' onClick={() => setGroupSettingsIsOpen(true)}>
        <span className='text-white'>{selectedChat.name}</span>
        {!selectedChat.isPrivate &&
        <span className='text-gray-300 ml-1'>({selectedChat.members.length} members)</span>}
      </div>}
      <GroupSettings open={groupSettingsIsOpen} handleClose={() => setGroupSettingsIsOpen(false)} />
    </div>
  </div>
}

export default Header;