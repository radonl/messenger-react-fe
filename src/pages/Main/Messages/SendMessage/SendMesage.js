import React, {useEffect, useState} from "react";
import {Picker} from 'emoji-mart'
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import {InsertEmoticon, Send} from "@material-ui/icons";
import Button from "@material-ui/core/Button";
import {useDispatch, useSelector} from "react-redux";
import {createChat, sendMessage, writingMessage} from "../../../../actions/chats/chats.actions";

const SendMessage = () => {
  const {selectedChat, newChatPartner} = useSelector(state => state.chats);
  const dispatch = useDispatch();
  const [message, setMessage] = useState('');
  const [emojiOpen, setEmojiOpen] = useState(false);
  const addEmoji = e => {
    let sym = e.unified.split('-');
    let codesArray = [];
    sym.forEach(el => codesArray.push('0x' + el));
    let emoji = String.fromCodePoint(...codesArray);
    setMessage(`${message}${emoji}`);
  };
  const messageChanged = (e) => {
    setMessage(e.target.value);
    dispatch(writingMessage(selectedChat.id));
  };
  useEffect(() => {
    setMessage('');
  }, [selectedChat])
  const user = useSelector(state => state.user);
  return <div className='h-135 flex justify-center'>
    <div className='w-524 flex justify-between'>
      <div className="rounded-full bg-gray-400 w-12 h-12 bg-cover" style={{backgroundImage: `url(${user.avatarPath})`}}/>
      <div className='flex flex-col w-3/4 relative'>
        <InsertEmoticon
          className='right-0 mr-2 mt-2 absolute text-gray-400 cursor-pointer'
          onClick={() => setEmojiOpen(!emojiOpen)}
        />
        <TextareaAutosize placeholder='Write a message'
                          rowsMin={3}
                          className='border-solid border-2 border-gray-100'
                          rowsMax={3}
                          value={message}
                          onChange={e => messageChanged(e)}
        />
        <div className='flex justify-end'>
          <Button color="primary"
                  className='w-5'
                  onClick={() => {
                    dispatch(
                      selectedChat.id ?
                        sendMessage({selectedChatId: selectedChat.id, message}):
                        createChat({partner: newChatPartner, message})
                    );
                    setMessage('');
                  }}
          >Send</Button>
        </div>
      </div>
      <div className={`${emojiOpen ? '' : 'hidden'} relative`}>
        <div className='emoji-picker absolute'>
          <Picker onSelect={addEmoji}/>
        </div>
      </div>
      <div className="rounded-full bg-gray-400 bg-cover w-12 h-12" style={{backgroundImage: `url(${selectedChat.avatarPath || newChatPartner.avatarPath})`}}/>
    </div>
  </div>;
};

export default SendMessage;