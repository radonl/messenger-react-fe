import React, {useEffect, useState} from "react";
import Message from "./Message/Message";
import SendMessage from "./SendMessage/SendMesage";
import {useSelector} from "react-redux";

const Messages = () => {
    const [lastEl, setLastEl] = useState();
    const {selectedChat, userWritingMessage, isPrepareToCreateChat} = useSelector(state => state.chats);
    const {messages} = useSelector(state => state.messages);
    console.log(messages[selectedChat.id], 'MESSAGES!!!');
    useEffect(() => {
        if(lastEl) {
            lastEl.scrollIntoView();
        }
    }, [messages[selectedChat.id] ? messages[selectedChat.id].length : 0, userWritingMessage]);
    return <div className='w-full'>
        <div className="flex flex-col items-center overflow-y-scroll messages-height">
            {selectedChat.id && messages && messages[selectedChat.id] && messages[selectedChat.id]
              .map((message, index) => <Message message={message} key={`${message.id}.${index}`} mtAuto={index === 0}/>)}
            <div ref={(el => setLastEl(el))} >
                {userWritingMessage}
            </div>
        </div>
        {(selectedChat.id || isPrepareToCreateChat) && <SendMessage/>}
    </div>
};

export default Messages;