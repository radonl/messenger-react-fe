import React from "react";
import {useSelector} from "react-redux";

const Message = ({message, mtAuto}) => {
  const users = useSelector(state => state.users);
  const sender = users.find(user => user.id === message.senderId);
  return sender ? <div className={`w-530 flex p-3 justify-between ${mtAuto && 'mt-auto'}`}>
    <div className="rounded-full bg-gray-400 bg-cover w-12 h-12" style={{backgroundImage: `url(${sender.avatarPath})`}} />
    <div className='w-3/4 pl-4'>
      <div>{sender.name}</div>
      <div>{message.message}</div>
    </div>
    <div>
      {message.displayedTime}
    </div>
  </div> : <div className='flex justify-center mt-auto'>
    <span className='text-blue-800'>{message.message}</span>
  </div>
};

export default Message;