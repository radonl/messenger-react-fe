import React from "react";
import TextField from "@material-ui/core/TextField";
import {useDispatch, useSelector} from "react-redux";
import {codeChanged} from "../../../actions/login/login.actions";

const ConfirmCode = () => {
    const { phone, code, authorizationFailed } = useSelector(state => state.login);
    const dispatch = useDispatch();
    return <div className="p-12 bg-white">
        <div className="text-center">
            <b className="text-lg">+380{phone}</b>
        </div>
        <div className="mt-5 text-center">
            <span className="text-gray-600">We've sent the code to your phone.</span>
        </div>
        <div className="mt-5 text-center">
            <span className="text-gray-600">Please enter the code below.</span>
        </div>
        <div className="flex">
            <TextField label="Enter your code"
                       className="w-full"
                       value={code}
                       onChange={e => dispatch(codeChanged(e.target.value))}
            />
        </div>
        {authorizationFailed && <div>
            <span className='text-red-800'>Incorrect code!</span>
        </div>}
    </div>
};

export default ConfirmCode;