import React from "react";
import SendCode from "./SendCode/SendCode";
import TelegramIcon from '@material-ui/icons/Telegram';
import ConfirmCode from "./ConfirmCode/ConfirmCode";
import {useDispatch, useSelector} from "react-redux";
import {authorization, sendCode} from "../../actions/login/login.actions";

const LoginPage = () => {
  const dispatch = useDispatch();
  const {
    requestInProgress,
    codeSent,
    requestFailed,
    phone,
    code
  } = useSelector(
    state => state.login
  );
  return <div className="main-wraper h-screen">
    <div className="bg-blue-500 w-100 h-48"/>
    <div className="flex justify-center">
      <div className="w-400 mt-m-120">
        <div className="flex p-5">
          <div className="flex items-center flex-grow">
            <div className="p-1 bg-blue-600 rounded-full">
              <TelegramIcon className="text-blue-100"/>
            </div>
            <b className="text-gray-200 ml-3">Messenger</b>
          </div>
          <div className="flex items-center justify-end">
            <button className="text-white w-10 font-bold flex justify-end"
                    onClick={() => codeSent ? dispatch(authorization({phone, code})) : dispatch(sendCode({phone}))}>
              <span>Next</span>
              <span className="text-lg ml-1">></span>
            </button>
          </div>
        </div>
        {codeSent ? <ConfirmCode/> : <SendCode/>}
      </div>
    </div>
  </div>
};

export default LoginPage;