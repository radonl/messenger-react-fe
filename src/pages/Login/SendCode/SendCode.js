import React from "react";
import TextField from "@material-ui/core/TextField";
import {connect, useDispatch, useSelector} from "react-redux";
import {phoneChanged} from "../../../actions/login/login.actions";

const SendCode = () => {
  const { phone } = useSelector(state => state.login);
  const dispatch = useDispatch();
  return <div className='p-12 bg-white'>
    <div>
      <b className="text-lg">Sign in</b>
    </div>
    <div className="mt-5">
      <span className="text-gray-600">Please enter your phone number:</span>
    </div>
    <div className="flex mt-3">
      <span className="mt-5 mr-2">+380</span>
      <TextField label="Phone number" value={phone} onChange={e => dispatch(phoneChanged(e.target.value))}/>
    </div>
  </div>
};

export default SendCode;