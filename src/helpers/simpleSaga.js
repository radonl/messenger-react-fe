import {put} from "@redux-saga/core/effects";

export default (cb, success, failed) => {
  return function* (action) {
    try {
      const response = yield cb(action.payload);
      yield put({
        type: success,
        payload: response
      })
    } catch (e) {
      yield put({
        type: failed,
        payload: e
      });
    }
  }
}